module Bowling 
(
    Frame(..)
    , scoreFromList
)
where
data Frame = Strike | Spare Int | Miss Int Int

scoreFromList :: [Int] -> [Frame]
scoreFromList [] = []
scoreFromList [10] = Strike : []
scoreFromList (x:y:xs)
    | x == 10 = Strike : scoreFromList(y:xs)
    | x + y == 10 = Spare x : scoreFromList(xs)
    | otherwise = Miss x y : scoreFromList(xs)

instance Show Frame where
    show Strike = " X"
    show (Spare x) = " " ++ show x ++ " /"
    show (Miss x y) = " " ++ show x ++ " " ++ (show y)