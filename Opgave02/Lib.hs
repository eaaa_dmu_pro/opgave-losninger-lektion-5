module Lib
( 
    printNumsTree
    , printStringsTree 
) where

import Tree
nums = [8,6,4,1,7,3,5]
numsTree = foldr treeInsert EmptyTree nums

strings = ["The", "rain", "in", "Spain", "stays", "mainly", "in", "the", "plain!"]
stringsTree = foldr treeInsert EmptyTree strings

printNumsTree :: IO ()
printNumsTree = putStrLn $ show numsTree

printStringsTree :: IO ()
printStringsTree = putStrLn $ printTree stringsTree 0

