module One 
(
    One(..)
    , Number(..)
    , addNumbers
    , subtractNumbers
    , multiplyNumbers
)
where

import Data.List
data One = One deriving (Show)
data Number = Number [One] 

instance Show Number where
    show (Number x) = show $ length x

instance Eq One where
    (==) One One = True

addNumbers :: Number -> Number -> Number
addNumbers (Number x) (Number y) = Number $ x ++ y

subtractNumbers :: Number -> Number -> Number
-- subtractNumbers (Number x) (Number []) = Number x 
-- subtractNumbers (Number []) _ = error "Can't handle negative numbers"
-- subtractNumbers (Number x) (Number y) = subtractNumbers (Number $ tail x) (Number $ tail y)
subtractNumbers (Number x) (Number y) = Number (x \\ y)

multiplyNumbers :: Number -> Number -> Number
multiplyNumbers _ (Number []) = Number []
multiplyNumbers (Number []) _ = Number []
multiplyNumbers (Number x) (Number [One]) = Number x 
multiplyNumbers (Number x) (Number y) = multiplyNumbers (addNumbers (Number x) (Number x)) (Number $ tail y)