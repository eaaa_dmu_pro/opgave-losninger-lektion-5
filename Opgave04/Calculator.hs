module Calculator 
(
    eval
)
where

    import ExprT
    eval:: Num a => ExprT a -> a
    eval (Lit a) = a
    eval (Add left right) = eval left + (eval right)
    eval (Mul left right) = eval left * (eval right)