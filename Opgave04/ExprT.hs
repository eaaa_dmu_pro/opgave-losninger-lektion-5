module ExprT where

data ExprT a = Lit a
    | Add (ExprT a) (ExprT a)
    | Mul (ExprT a) (ExprT a)
    deriving (Show, Eq)

instance Functor ExprT where
    fmap f (Lit a) = Lit (f a)
    fmap f (Add left right) = Add (fmap f left) (fmap f right)
    fmap f (Mul left right) = Mul (fmap f left) (fmap f right)
