module Lib
( 
    someFunc
    , exp1
    , exp2
    , test1
    , test2
) where

import ExprT
import Calculator
someFunc :: IO ()
someFunc = putStrLn "someFunc"

exp1 = Mul (Add (Lit 2.3) (Lit 3.3)) (Lit 4.3)
exp2 = Add (Mul (Lit 2) (Lit 4)) (Mul (Lit 3) (Lit 4))

test1 = eval exp1
test2 = eval exp2
